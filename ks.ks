server {
	
	listen 80;
        listen [::]:80;
        server_name    ks.;
        root  /var/www/html/ks;
        index index.php index.html index.htm ;
location ~ \.(php|htm|html|phtml)$ {
       try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass unix:/var/run/php/php7.2-fpm.sock;
        fastcgi_index index.php;
        include /etc/nginx/fastcgi.conf;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        include fastcgi_params;
    }

        
}
